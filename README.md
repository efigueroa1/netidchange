### Name Change Script 

Script to change LDAP and AD attributes after a name change. 

# Requirements 
- Python3
- Python modules: ldap3, getpass, re

# Usage

> python3 ./nameChange.py
