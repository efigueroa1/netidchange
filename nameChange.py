from ldap3 import Server, Connection, ALL, SUBTREE, MODIFY_REPLACE, MODIFY_DELETE, MODIFY_ADD, NTLM, ALL_ATTRIBUTES
import sys, subprocess, platform
import getpass, re

ldapHost = 'ldap.csub.edu'
base = 'dc=csub,dc=edu'
searchAttributes = ['employeeNumber','seeAlso','userPassword','uid','cn','sn','objectClass']
domain = 'ad.csub.edu'
adbase = 'dc=ad,dc=csub,dc=edu'
adsearchAttributes = ['employeeNumber','samaccountname','uid','userprincipalname','proxyaddresses','DistinguishedName']

def yesOrNo(question):
    while "the answer is invalid":
        reply = input('[!] '+question+' (y/n): ').lower().strip()
        if reply[0] == 'y':
            return True
        if reply[0] == 'n':
            return False

class User:
    def __init__(self, ldapUser, adUser, ldapPass, adPass, adServer, ldapServer):
        self.ldapUser   = ldapUser
        self.adUser     = adUser
        self.ldapPass   = ldapPass
        self.adPass     = adPass
        self.ldapServer = ldapServer
        self.adServer   = adServer

def setConnection(netid):
    ldapHost = 'ldap.csub.edu'
    base = 'dc=csub,dc=edu'
    searchAttributes = ['employeeNumber','seeAlso','userPassword','uid','cn','sn','objectClass']
    domain = 'ad.csub.edu'
    adbase = 'dc=ad,dc=csub,dc=edu'
    adsearchAttributes = ['employeeNumber','samaccountname','uid','userprincipalname','proxyaddresses']
    #LDAP Connection
    ldapServer = Server(ldapHost, use_ssl=True, get_info=ALL)
    tempserver = Server(ldapHost, port=389,use_ssl=False, get_info=ALL)
    #AD Connection
    adServer = Server(domain,port=636, use_ssl=True, get_info=ALL)
    #Create connection
    tempConn = Connection(tempserver, auto_bind='NONE', version=3, authentication='ANONYMOUS', client_strategy='SYNC', auto_referrals=True, check_names=True, read_only=False, lazy=False, raise_exceptions=False)
    tempConn.bind()
    userFilter = '(uid='+netid+')'
    tempConn.search(search_base=base, search_filter=userFilter, search_scope=SUBTREE, attributes=searchAttributes)
    user = tempConn.entries[0].entry_dn
    tempConn.unbind()
    aduser = domain+'\\'+netid+'-admin'
    ldapPass = getpass.getpass('Password for %s: '%netid)
    adPass = getpass.getpass('Password for %s: '%aduser)
    return User(user, aduser, ldapPass, adPass, adServer, ldapServer)

def getUser():
    #get customer info
    customerFound = False
    while not customerFound:
        emplID = input("enter employeeNumber of customer: ")
        userfilter = '(employeenumber='+emplID+')'
        ldap.search(search_base=base, search_filter=userfilter, search_scope=SUBTREE, attributes=searchAttributes)
        found = len(ldap.entries)
        if len(ldap.entries) == 1:
            customerFound = True
            print('[#] User found')
        elif len(ldap.entries) > 1:
            print('[!] Multiple Users found - Unable to proceed')
            exit
        elif len(ldap.entries) == 0:
            print("[!] No customer by that employeeNumber found")
            customerFound = False
    return ldap.entries[0]

def nameChangeOptions(customer):
    print("[#] Current Last Name: %s" % customer.sn.value)
    print("[#] Current UID: %s" % customer.uid.value)
    validChoice = False
    while not validChoice:
        for i in range(len(customer.cn)):
            print('%s: %s' % (i,customer.cn[i]))
        other = len(customer.cn)
        print('%s: Other Cosmetic Name Change' % other)
        choice = int(input("Select New Name: "))
        if choice >= 0 and choice < other:
            print("[#] New Name Selected: %s" % customer.cn[choice])
            if yesOrNo("Continue with this name?"):
                return customer.cn[choice]
        elif choice == other:
            print("[!] ENTER FIRST AND LAST NAME TO BASE THE NEW NETID ON. USE CAUTION. NO VALIDATION PROVIDED.")
            print("[!] Entering 'J Doe' or 'John Doe' will create a netid starting with 'jdoe'")
            name = input("Custom Name to generate NetID: ")
            print("[#] New Name Inputted: %s" % name)
            if yesOrNo("Continue with this name?"):
                return customer.cn[choice]
        else:
            print("[!] INVALID RESPONSE. Must be numbered response listed.")

def findNextNetID(name):
    baseUID=(name.split()[0][0]+name.split()[1].split('-')[0]).lower()
    userfilter='(uid='+baseUID+'*)'
    print('[#] Searching for next NetID...')
    ldap.search(search_base=base, search_filter=userfilter, search_scope=SUBTREE, attributes=searchAttributes)
    #no results found
    if len(ldap.entries) > 0:
        num = 1
        nameRegex = re.compile('^%s[0-9]'%baseUID,re.IGNORECASE)
        numbers = re.compile('([0-9]*$)')
        for user in ldap.entries:
            if nameRegex.match(user.uid.value):
                nextInt = int(numbers.search(user.uid.value).group())+1
                if nextInt > num:
                    num = nextInt
        newNetID = baseUID+str(num)
    else:
        newNetID = baseUID
    print("[#] New NetID: %s" % newNetID)
    return newNetID

def updateLDAP(customer, netID):
    #AD ITEMS
    userFilter = '(employeeNumber='+customer.employeeNumber.value+')'
    ad.search(search_base=adbase, search_filter=userFilter, search_scope=SUBTREE)
    newDN = re.sub("CN=[a-zA-Z0-9]*,",'CN='+netID+',',ad.entries[0].entry_dn)
    #LDAP ITEMS
    newMail = netID+'@csub.edu' 
    if customer.userPassword:
        ldap.modify(customer.entry_dn, {'userPassword': [(MODIFY_DELETE, [])]})
        print('[#] LDAP userPassword Removed')
    if not "inetLocalMailRecipient" in customer.objectClass.value:
        ldap.modify(customer.entry_dn, {'objectClass': [(MODIFY_ADD, ["inetLocalMailRecipient"])]})
    ldap.modify(customer.entry_dn,
            {'seeAlso'  : [(MODIFY_REPLACE, [newDN])],
             'uid'      : [(MODIFY_REPLACE, [netID])],
             'mail'     : [(MODIFY_REPLACE, [newMail])],
             'mailLocalAddress' : [(MODIFY_ADD, [newMail])]
             })
    print("[#} LDAP Attributes Updated")


def updateAD(customer, netID):
    if platform.system() == 'Linux':
        template = './templates/powershell.template'
        outputFile = './output/'+newNetID+'.ps1'
        autoRun = False
    if platform.system() == 'Windows':
        template = 'C:\\Scripts\\netidchange\\templates\\powershell.template'
        outputFile = "C:\\Scripts\\netidchange\\output\\"+newNetID+".ps1"
        autoRun = True
    with open(template, 'r') as infile:
        line = infile.read()
        line = re.sub('EMPLOYEENUMBER',customer.employeeNumber.value,line)
        line = re.sub('NEWNETID',netID,line)
        with open(outputFile, 'a') as output:
            output.write(line) 
    print("[#] Powershell Script Created")
    if autoRun:
        p = subprocess.Popen(["powershell.exe", outputFile], stdout=sys.stdout)
        p.communicate()
    else:
        print("[#] Powershell file created in output directory")

#Create connection
user = input("Enter your NetID: ")
user = setConnection(user)

# binds
print('[#] Binding to Directories')
try:
    ldap    = Connection(user.ldapServer, user.ldapUser, user.ldapPass, auto_bind=True, raise_exceptions=True)
    ad      = Connection(user.adServer, user.adUser, user.adPass, authentication=NTLM, auto_bind=True)
except:
    print("[!] Unable to bind to directories")
    input()
    sys.exit()

customer = getUser()
newName = nameChangeOptions(customer)
newNetID = findNextNetID(newName)
if yesOrNo("Update LDAP Entry?"):
    updateLDAP(customer, newNetID)
if yesOrNo("Update Active Directory Entry?"):
    updateAD(customer, newNetID)
